package inputformat;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class SumReducer extends Reducer<Key, Value, Key, DoubleWritable> {

    @Override
    protected void reduce(Key key, Iterable<Value> values, Context context) throws IOException, InterruptedException {
        double acum = 0;
        for (Value v : values) {
            acum += v.getFirstValue() + v.getSecondValue();
        }
        context.write(key, new DoubleWritable(acum));
    }
}
