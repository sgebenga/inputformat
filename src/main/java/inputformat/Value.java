package inputformat;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Value implements Writable {

    private double firstValue;
    private double secondValue;

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeDouble(firstValue);
        dataOutput.writeDouble(secondValue);
    }

    public void readFields(DataInput dataInput) throws IOException {
        firstValue = dataInput.readDouble();
        secondValue = dataInput.readDouble();
    }

    public double getFirstValue() {
        return firstValue;
    }

    public void setFirstValue(double firstValue) {
        this.firstValue = firstValue;
    }

    public double getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(double secondValue) {
        this.secondValue = secondValue;
    }

    @Override
    public String toString() {
        return String.format("%s,%s", firstValue, secondValue);
    }
}
