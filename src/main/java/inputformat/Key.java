package inputformat;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Key implements WritableComparable<Key> {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy/MM/dd");

    private int id;
    private Date date;
    private int zip;

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(id);
        dataOutput.writeUTF(FORMAT.format(date));
        dataOutput.writeInt(zip);
    }

    public void readFields(DataInput dataInput) throws IOException {
        id = dataInput.readInt();
        parseDate(dataInput.readUTF());
        zip = dataInput.readInt();
    }

    private void parseDate(String s) throws IOException {
        try {
            date = FORMAT.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int compareTo(Key o) {
        return id - o.id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String s) {
        try {
            parseDate(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String toString() {
        return String.format("%s,%s,%s", id, FORMAT.format(date), zip);
    }
}
