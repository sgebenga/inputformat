package inputformat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.lib.InputSampler;
import org.apache.hadoop.mapred.lib.TotalOrderPartitioner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.net.URI;

public class Application extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new Application(), args);
        System.exit(exitCode);
    }

    public int run(String[] strings) throws Exception {
        Configuration configuration = getConf();
        Job job = Job.getInstance(configuration, "sum application");

        FileInputFormat.addInputPath(job, new Path(strings[0]));

        Path out = new Path(strings[1]);

        FileSystem fsy = FileSystem.get(configuration);
        if (fsy.exists(out)) {
            fsy.delete(out, true);
        }
        FileOutputFormat.setOutputPath(job, out);

        job.setMapperClass(SumMapper.class);
        job.setInputFormatClass(SumInputFormat.class);

        job.setOutputKeyClass(Key.class);
        job.setOutputValueClass(Value.class);

        job.setReducerClass(SumReducer.class);
        job.setNumReduceTasks(2);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
