package inputformat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SumRecordReader extends RecordReader<Key, Value> {

    private FSDataInputStream fsDataInputStream;
    private BufferedReader bufferedReader;
    private Key key = new Key();
    private Value value = new Value();
    private long start;
    private long end;
    private long pos;

    @Override
    public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        FileSplit fileSplit = (FileSplit) inputSplit;
        Configuration configuration = taskAttemptContext.getConfiguration();
        Path path = fileSplit.getPath();
        FileSystem fs = path.getFileSystem(configuration);
        fsDataInputStream = fs.open(path);
        bufferedReader = new BufferedReader(new InputStreamReader(fsDataInputStream));
        start = fileSplit.getStart();
        end = start + fileSplit.getLength();
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        boolean next = false;
        String line = bufferedReader.readLine();
        if (line != null) {
            String[] fields = line.split(",");
            key.setId(parseInt(fields[0]));
            key.setDate(fields[1]);
            key.setZip(parseInt(fields[2]));
            value.setFirstValue(parseDouble(fields[3]));
            value.setSecondValue(parseDouble(fields[4]));
            next = true;
            pos += line.length();
        }
        return next;
    }

    private int parseInt(String s) {
        return Integer.parseInt(s);
    }

    private double parseDouble(String s) {
        return Double.parseDouble(s);
    }

    @Override
    public Key getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public Value getCurrentValue() throws IOException, InterruptedException {
        return value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        return Math.min(1.0f, (pos - start) / (end - start));
    }

    @Override
    public void close() throws IOException {
        fsDataInputStream.close();
    }
}
