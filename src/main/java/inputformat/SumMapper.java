package inputformat;

import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SumMapper extends Mapper<Key, Value, Key, Value> {

    @Override
    protected void map(Key key, Value value, Context context) throws IOException, InterruptedException {
        context.write(key, value);
    }
}
